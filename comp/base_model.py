import enum
import json
import warnings
import time
from abc import abstractmethod
from typing import Set, List, Type, TypeVar, Tuple, Dict, Any, Optional, Union, Generic, ClassVar, Callable, Iterator, cast, FrozenSet

import comp.file_op
from comp.config import Config
from comp.constants import AlreadyInitializedError, NotInitializedError
from comp.log import ClassLogger


"""
Allowed types of columns for BaseModel classes. It's very important that this only includes
immutable types like `str` or `tuple` (but not `list`) because of the pattern of how modifications
are tracked and how default values are instantiated.

It's also important that any type can be converted to/from json.
"""
ColValue = Union[int, float, str, Tuple[int]]
_ColValue = TypeVar('_ColValue', bound=ColValue)

_BaseModel = TypeVar('_BaseModel', bound='BaseModel')  # cant use forward reference here
TableClass = Type['BaseModel']  # represents subclasses of BaseModel declared as tables in DB.
SetAttrHook = Callable[[_BaseModel, _ColValue, _ColValue], None]
JSONDict = Dict[str, Any]


class InvalidIDError(Exception):
  def __init__(self, cls: TableClass, id: int):
    super().__init__('Invalid id %d for table %s' % (id, cls.get_table_name()))


class ReadOnlyError(Exception):
  pass


class NotLoadedError(Exception):
  pass


class StateError(Exception):
  pass


class ValidationError(Exception):
  pass


class CorruptError(Exception):
  pass


class NotRegisteredError(Exception):
  pass


class LoadingStrategy(enum.Enum):
  IMMEDIATE = 1 # All object instances loaded when Database.initialize() is called
  EXPLICIT = 2 # Objects loaded whenever get(id, allow_load=True) is called


class _State(enum.Enum):
  """
  States a database object can have.
  Modeled after SQLAlchemy: https://docs.sqlalchemy.org/en/13/orm/session_state_management.html
  The only major difference between these and SQLAlchemy states is:
    1. Persistent objects with non-flushed changes have a new state called _PERSISTENT_MODIFIED
    2. Detached objects don't have a state (they are essentially non-existent)
  """
  _PERSISTENT = 1
  _PENDING = 2
  _PERSISTENT_MODIFIED = 3
  _DELETED = 4


class Field(Generic[_ColValue]):
  """
  Attributes:

    setattr_hook: If set, will be called before __setattr__ when this field is being changes. Its
      called with (self, old_value, new_value) where `self` is instance of column being set.
  """
  field_type: Type[_ColValue]
  default: Optional[_ColValue]
  read_only: bool
  required: bool
  setattr_hook: Optional[SetAttrHook]

  def __init__(
      self,
      field_type: Type[_ColValue],
      required: bool=False,
      default: Optional[_ColValue]=None,
      read_only: bool=False,
      setattr_hook: SetAttrHook=None) -> None:
    self.default = default
    self.field_type = field_type
    self.required = required
    self.read_only = read_only
    if self.default is not None and not isinstance(self.default, self.field_type):
      raise TypeError()
    self.setattr_hook = setattr_hook

  def __eq__(self, obj: Any) -> bool:
    if self.__class__ != obj.__class__:
      return False
    return self.field_type == obj.field_type and self.default == obj.default and self.read_only == obj.read_only and self.required == obj.required and self.setattr_hook == obj.setattr_hook

  def from_value(self, value: Any) -> Any:
    if self.field_type == tuple and isinstance(value, list):
      return tuple(value)
    else:
      return value


@ClassLogger()
class BaseModel(object):
  # Class attributes (each subclass has separate value)
  # class attributes valid for all subclasses
  __columns__: ClassVar[Dict[str, Field]] = {}
  _loading_strategy: ClassVar[LoadingStrategy] = LoadingStrategy.EXPLICIT
  _is_registered: ClassVar[bool] = False
  log: ClassVar[ClassLogger]
  # class attributes valid for registered classes only
  _next_object_id: ClassVar[int]
  _obj_ids_in_session: ClassVar[Set[int]] = set()
  _obj_ids_in_database: ClassVar[Set[int]]
  _record_list_loaded: ClassVar[bool]
  _table_name: ClassVar[str] # must be defined for class to be registered

  # Instance attributes
  _id: int
  _state: _State

  def __init__(self, _existing_object_id: int=None, **column_values: Dict[str, Any]) -> None:
    if not self.__class__.is_registered():
      raise NotRegisteredError()
    if not self.__class__.is_initialized():
      raise NotInitializedError()

    # for each column value passed, set the corresponding instance attribute
    columns = self.__class__.__columns__
    for name, value in column_values.items():
      try:
        field = columns[name]
        self.__dict__[name] = field.from_value(value)
      except KeyError:
        raise AttributeError('Column "%s" passed to __init__ was not a valid column' % name)

    # for each column not explicitly passed, set it to its default value
    for name, field in columns.items():
      if name not in column_values:
        if field.default is None and field.required:
          raise AttributeError('Missing value for required column: "%s"' % name)
        else:
          # use default for any values not provided
          self.__dict__[name] = field.default

    if not _existing_object_id:
      # if no id passed, assume this is a new object and assign it the next available ID.
      self._id = self.__class__._next_object_id
      self._state = _State._PENDING
      self.__class__._next_object_id += 1
      self.__class__._obj_ids_in_session.add(self._id)
    else:
      # otherwise, ensure we don't already have an instance for this record.
      assert not self.__class__.has_model_instance(_existing_object_id)
      self._id = _existing_object_id
      self._state = _State._PERSISTENT

    self.__class__.get_instance_map()[self._id] = self

  def __init_subclass__(cls, table_name: str=None, loading_strategy: LoadingStrategy=None) -> None:
    super().__init_subclass__()

    # make class column dict inherit from parent classes
    column_dict = {}
    for base in cls.__bases__:
      if issubclass(base, BaseModel):
        column_dict.update(base.__columns__)
    column_dict.update(cls.__columns__)
    cls.__columns__ = column_dict

    # if has table name, give subclass own copy of class variables
    if table_name is not None:
      cls._obj_ids_in_session = set()
      cls._obj_ids_in_database = set()
      cls._next_object_id = 1
      cls._record_list_loaded = False
      cls._table_name = table_name
    if loading_strategy is not None:
      cls._loading_strategy = loading_strategy

  @classmethod
  def _load_record_list(cls) -> None:
    """Reads list of objects contained in filesystem. This must be called before doing anything."""
    assert cls.is_registered
    if cls.is_initialized():
      raise AlreadyInitializedError()
    cls._obj_ids_in_database.clear()
    cls._obj_ids_in_session.clear()

    try:
      database_ids = BaseModelWriter.read_record_list_file(cls)
      # reuse same object so references will update as well
      cls._obj_ids_in_database.update(database_ids)
      cls._obj_ids_in_session.update(database_ids)
    except FileNotFoundError:
      cls.log.wrn('no record list found for class: %s, using empty list', cls.__name__)
    except CorruptError as ex:
      # TODO: Better handling for this situation
      cls.log.err('record list for class %s is corrupt', cls.__name__)
      raise ex

    cls._next_object_id = max(cls._obj_ids_in_session) + 1 if cls._obj_ids_in_session else 1
    cls._record_list_loaded = True

    if cls._loading_strategy == LoadingStrategy.IMMEDIATE:
      cls.get_all()

  @classmethod
  def _load_object(cls: Type[_BaseModel], id: int) -> _BaseModel:
    assert id not in cls.get_instance_map()
    column_values = BaseModelWriter.read_record_file(cls, id)
    return cls(_existing_object_id=id, **column_values)

  @classmethod
  def is_registered(cls) -> bool:
    return cls._is_registered

  @classmethod
  def is_initialized(cls) -> bool:
    return cls._record_list_loaded

  @classmethod
  def get_columns(cls) -> Dict[str, Field]:
    return cls.__columns__

  @classmethod
  def get_loading_strategy(cls) -> LoadingStrategy:
    return cls._loading_strategy

  @classmethod
  def get_table_name(cls) -> str:
    if not cls.is_registered():
      raise NotRegisteredError()
    return cls._table_name

  @classmethod
  def get(cls: Type[_BaseModel], id: int, allow_load: bool=False) -> _BaseModel:
    if not cls.is_registered():
      raise NotRegisteredError()
    if not cls.is_initialized():
      raise NotInitializedError()
    if not id in cls._obj_ids_in_session:
      raise InvalidIDError(cls, id)
    if id in cls.get_instance_map():
      return cls.get_instance_map()[id]

    if allow_load:
      return cls._load_object(id)
    else:
      raise NotLoadedError()

  @classmethod
  def get_instance_map(cls: Type[_BaseModel]) -> Dict[int, _BaseModel]:
    if not cls.is_registered():
      raise NotRegisteredError()
    if not cls.is_initialized():
      raise NotInitializedError()
    return Database.get_instance_map(cls)

  @classmethod
  def get_all(cls: Type[_BaseModel]) -> Dict[int, _BaseModel]:
    if not cls.is_registered():
      raise NotRegisteredError()
    return {id: cls.get(id, allow_load=True) for id in cls._obj_ids_in_session}

  @classmethod
  def get_loaded_ids(cls) -> Set[int]:
    if not cls.is_registered():
      raise NotRegisteredError()
    return {id for id in cls.get_instance_map().keys()}

  @classmethod
  def get_session_ids(cls) -> Set[int]:
    if not cls.is_registered():
      raise NotRegisteredError()
    if not cls.is_initialized():
      raise NotInitializedError()
    return {id for id in cls._obj_ids_in_session}

  @classmethod
  def get_added(cls: Type[_BaseModel]) -> Set[_BaseModel]:
    if not cls.is_registered():
      raise NotRegisteredError()
    if not cls.is_initialized():
      raise NotInitializedError()
    return {cls.get_instance_map()[id] for id in cls._obj_ids_in_session.difference(cls._obj_ids_in_database)}

  @classmethod
  def get_modified(cls: Type[_BaseModel]) -> Set[_BaseModel]:
    if not cls.is_registered():
      raise NotRegisteredError()
    if not cls.is_initialized():
      raise NotInitializedError()
    return {obj for _, obj in cls.get_instance_map().items() if obj.is_modified()}

  @classmethod
  def get_removed(cls: Type[_BaseModel]) -> Set[_BaseModel]:
    if not cls.is_registered():
      raise NotRegisteredError()
    if not cls.is_initialized():
      raise NotInitializedError()
    return {cls.get_instance_map()[id] for id in cls._obj_ids_in_database.difference(cls._obj_ids_in_session)}

  @classmethod
  def has_record_in_database(cls, id: int) -> bool:
    """
    Whether given id exists in the record list stored in the filesystem. This may be different from
    `has_record_in_session` which reflects uncommitted changes like removals and additions.
    """
    if not cls.is_registered():
      raise NotRegisteredError()
    if not cls.is_initialized():
      raise NotInitializedError()
    return id in cls._obj_ids_in_database

  @classmethod
  def has_record_in_session(cls, id: int) -> bool:
    """
    Whether given id exists in the record list in current session. This may be different from
    `has_record_in_database` which corresponds to the record list currently saved in the filesystem.

    After committing changes, these two methods should agree
    """
    if not cls.is_registered():
      raise NotRegisteredError()
    if not cls.is_initialized():
      raise NotInitializedError()
    return id in cls._obj_ids_in_session

  @classmethod
  def has_model_instance(cls, id: int) -> bool:
    """
    True if individual record is loaded into memory. This includes objects created in session.
    Note that this can be true even if `has_record_in_database` and/or `has_record_in_session` is
    false if there is an uncommitted delete of an object that was previously loaded or created.
    """
    return id in cls.get_instance_map()

  @classmethod
  def _reset_class_state(cls) -> None:
    """Purges all object data for this class."""
    cls._obj_ids_in_session.clear()
    cls._obj_ids_in_database.clear()
    cls._next_object_id = 1
    cls._record_list_loaded = False

  @classmethod
  def _validate_changes(cls) -> None:
    for obj in cls.get_added():
      obj._validate()
    for obj in cls.get_modified():
      obj._validate()

  @classmethod
  def _commit_changes(cls) -> None:
    """Pushes changes to filesystem and resets object states."""
    to_remove = cls.get_removed()
    to_save = cls.get_added().union(cls.get_modified())
    for obj in to_remove:
      obj._commit_remove()
    for obj in to_save:
      obj._commit_save()
    if cls._obj_ids_in_session != cls._obj_ids_in_database:
      BaseModelWriter.write_record_list_file(cls, cls.get_session_ids())

    # Reset object states:
    cls._obj_ids_in_database.clear()
    cls._obj_ids_in_database.update(cls._obj_ids_in_session)
    for obj in to_remove:
      obj._purge()
    for obj in to_save:
      obj._state = _State._PERSISTENT
    cls._next_object_id = max(cls._obj_ids_in_database) + 1 if cls._obj_ids_in_database else 1

  @property
  def id(self) -> int:
    return self._id

  def __repr__(self) -> str:
    return '%s(%s)' % (self.__class__.__name__, str(self.id) if hasattr(self, 'id') else 'PURGED')

  def __setattr__(self, key: str, value: Any) -> None:
    """
    Attributes beginning with '_' behave as normal. For all other attributes, they can only be
    changed if they are declared as Field's and not read_only.
    """
    if key.startswith('_'):
      return super().__setattr__(key, value)

    if self._get_state() == _State._DELETED:
      raise StateError('Cannot modify attributes of removed objects')

    try:
      field = self.__class__.__columns__[key]
      if field.read_only:
        raise ReadOnlyError()
      if field.setattr_hook:
        field.setattr_hook(self, getattr(self, key), value)
      self.__dict__[key] = value

      if self._get_state() == _State._PERSISTENT:
        self._set_modified()

    except KeyError:
      raise AttributeError()

  def is_modified(self) -> bool:
    return self._get_state() == _State._PERSISTENT_MODIFIED

  def remove(self) -> None:
    if not hasattr(self, '_id'):
      raise StateError()
    if not self.__class__.has_record_in_session(self.id):
      raise StateError()

    self.__class__._obj_ids_in_session.remove(self.id)
    self._state = _State._DELETED

  def _get_state(self) -> _State:
    return self._state

  def _set_modified(self) -> None:
    self._state = _State._PERSISTENT_MODIFIED

  def _commit_remove(self) -> None:
    assert self._get_state() == _State._DELETED
    BaseModelWriter.remove_record_file(self.__class__, self.id)

  def _commit_save(self) -> None:
    assert self._get_state() != _State._DELETED
    obj_dict = {}
    for key, field in self.__class__.get_columns().items():
      obj_dict[key] = getattr(self, key)
    BaseModelWriter.write_record_file(self.__class__, self.id, obj_dict)

  def _purge(self) -> None:
    self.__class__.get_instance_map().pop(self.id)
    del self._id

  def _validate(self) -> None:
    """
    For every column, we do two checks:
      1. there must be a key in the instance dict with the same name
      2. it's value must have correct type or None (if non-required)
    Because there is always a class attribute with same name as column (which is set as instance
    attribute), it's important that we check for the key in the instance dict and not by checking
    hasattr, because the latter will always return True due to the presence of the class attribute.

    This issue might be indicative of poor design choices...
    """
    assert self._get_state() != _State._DELETED
    for key, field in self.__class__.__columns__.items():
      # must check __dict__ and not hasattr due to reason given above
      if key not in self.__dict__:
        raise ValidationError('missing entry for column "%s"' % key)
      value =  self.__dict__.get(key)
      if value is None:
        if field.required:
          raise ValidationError('required column "%s" has value "None"' % key)
      else:
        if not isinstance(value, field.field_type):
          raise ValidationError('column "%s" has incorrect type "%s" (expected "%s")' % (key, type(value), field.field_type))


class BaseModelWriter(object):
  """
  Thin wrapper around reading/writing methods for record files and record lists.

  This class primarily exists to make tests much easier to write.
  """
  @classmethod
  def read_record_list_file(cls, table_class: TableClass) -> Set[int]:
    """
    Raises:
      FileNotFoundError
      CorruptError
      PermissionError
      ?
    """
    filename = Config.get().get_record_list_filename(table_class.get_table_name())
    contents = comp.file_op.read_file(filename)
    try:
      return {int(id.strip()) for id in contents.split(',') if id}
    except ValueError:
      raise CorruptError()

  @classmethod
  def read_record_file(cls, table_class: TableClass, id: int) -> JSONDict:
    """
    Raises:
      FileNotFoundError
      CorruptError
      PermissionError
      ?
    """
    filename = Config.get().get_record_filename(table_class.get_table_name(), id)
    contents = comp.file_op.read_file(filename)
    try:
      return json.loads(contents)
    except json.decoder.JSONDecodeError:
      raise CorruptError()

  @classmethod
  def write_record_list_file(cls, table_class: TableClass, ids: Set[int]) -> None:
    filename = Config.get().get_record_list_filename(table_class.get_table_name())
    contents = ','.join([str(id) for id in ids])
    comp.file_op.write_file(filename, contents)

  @classmethod
  def write_record_file(cls, table_class: TableClass, id: int, obj_dict: JSONDict) -> None:
    filename = Config.get().get_record_filename(table_class.get_table_name(), id)
    contents = json.dumps(obj_dict, indent=2)
    comp.file_op.write_file(filename, contents)

  @classmethod
  def remove_record_file(cls, table_class: TableClass, id: int) -> None:
    filename = Config.get().get_record_filename(table_class.get_table_name(), id)
    comp.file_op.rename_to_temp(filename)

  @classmethod
  def cleanup(cls)-> None:
    # TODO:
    pass


class Database(object):
  _cls_to_instance_map: Dict[TableClass, Dict[int, BaseModel]] = {}
  _table_names: Set[str] = set()

  def __init__(self) -> None:
    raise Exception('Cannot be instantiated')

  @staticmethod
  def get_instance_map(table_class: Type[_BaseModel]) -> Dict[int, _BaseModel]:
    return cast(Dict[int, _BaseModel], Database._cls_to_instance_map[table_class])

  @staticmethod
  def has_registered_class(table_class: TableClass) -> bool:
    return table_class in Database._cls_to_instance_map

  @staticmethod
  def initialize() -> None:
    for table_class in Database._cls_to_instance_map:
      table_class._load_record_list()

  @staticmethod
  def commit_changes() -> None:
    for table_class in Database._cls_to_instance_map:
      table_class._validate_changes()

    for table_class in Database._cls_to_instance_map:
      table_class._commit_changes()
    BaseModelWriter.cleanup()

  @staticmethod
  def reset() -> None:
    for table_class, instance_map in Database._cls_to_instance_map.items():
      instance_map.clear()
      table_class._reset_class_state()

  @staticmethod
  def register_table_class(table_class: Type[_BaseModel]) -> Type[_BaseModel]:
    """
    This is intended to be used as a decorator for classes to be read/written to database.

    Example:

      @Database.register_table_class
      class MyModel(BaseModel, table_name='base_model'):
        __columns__ = {
          ...
        }

    The "table_name" argument is crucial. Without this, trying to register a class will raise an
    exception.
    """
    if not hasattr(table_class, '_table_name'):
      raise Exception('Missing table name for registered class: %s' %  table_class.__name__)
    table_name = table_class._table_name

    if table_class in Database._cls_to_instance_map:
      raise Exception('Duplicate table class: %s' % table_class.__name__)
    if table_name in Database._table_names:
      raise Exception('Duplicate table name: %s' % table_name)

    table_class._is_registered = True
    Database._cls_to_instance_map[table_class] = {}
    Database._table_names.add(table_name)
    return table_class

  @staticmethod
  def unregister_table_class(table_class: Type[_BaseModel]) -> Type[_BaseModel]:
    """
    This is really only intended for testing purposes.
    """
    warnings.warn('This method should only be used in tests!')
    assert hasattr(table_class, '_table_name')
    table_name = table_class._table_name

    table_class._reset_class_state()

    if table_name in Database._table_names:
      Database._table_names.remove(table_name)
    if table_class in Database._cls_to_instance_map:
      Database._cls_to_instance_map.pop(table_class)
    table_class._is_registered = False
    return table_class
