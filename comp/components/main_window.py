import gi.repository
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio, WebKit2

import pkgutil
from typing import Any, List, Optional, Dict, Set, TypeVar, Type, Callable, Tuple, cast


import comp.constants
from comp import action_service
from comp.base_model import Database
from comp.config import Config
from comp.log import ClassLogger
from comp.models import Folder, Note, Node, NoteConnector
from comp.preferences_dialog import PreferencesDialog
from comp.components.note_editor import NoteEditor
from comp.components.note_viewer import NoteViewer
from comp.components.nav_pane import NavPane


@ClassLogger()
@Gtk.Template.from_string((pkgutil.get_data('views', 'main-window.ui') or bytes()).decode('UTF-8'))
class MainWindow(Gtk.ApplicationWindow):
  __gtype_name__ = 'MainWindow'
  log: ClassLogger

  header_bar = cast(Gtk.HeaderBar, Gtk.Template.Child())
  header_paned = cast(Gtk.Paned, Gtk.Template.Child())
  nav_pane = cast(NavPane, Gtk.Template.Child())
  note_editor = cast(NoteEditor, Gtk.Template.Child())
  note_viewer = cast(NoteViewer, Gtk.Template.Child())
  root_paned = cast(Gtk.Paned, Gtk.Template.Child())
  _preferences_dialog: PreferencesDialog

  def __init__(self) -> None:
    super().__init__()
    self.set_property('title', comp.constants.APPLICATION_TITLE)

    # setup preferences dialog
    self._preferences_dialog = PreferencesDialog()
    self._preferences_dialog.set_transient_for(self)

    # ensure paned in header bar and paned in app content area are always at same position
    self.header_paned.bind_property('position', self.root_paned, 'position', GObject.BindingFlags.BIDIRECTIONAL)

    # Setup actions
    for label, method, enabled, accel in [
      ('save-note', self.save_note, False, '<Ctrl>S'),
      ('new-note', self.new_note, True, '<Alt>n'),
      ('new-folder', self.new_folder, True, '<Alt><Shift>N'),
      ('remove-object', self.remove_object, False, '<Alt>Delete'),
      ('hide-nav-pane', self.hide_nav_pane, True, None),
    ]:
      action = Gio.SimpleAction.new(label)
      action.set_enabled(enabled)
      action.connect('activate', method)
      action_service.register(action, accel and [accel] or [])

  def _update_title(self) -> None:
    note = self.note_editor.note
    if not note:
      self.header_bar.set_title('')
    elif self.note_editor.is_modified:
      self.header_bar.set_title('*' + note.title)
    else:
      self.header_bar.set_title(note.title)

  def _update_rendered(self) -> None:
    self.log.low()
    self.note_viewer.render_from_markdown(self.note_editor.get_contents())

  @Gtk.Template.Callback('note-modified-changed')
  def _on_note_modified_change(self, _: NoteEditor) -> None:
    """Template handler."""
    self.log.low()
    action_service.set_enabled('save-note', self.note_editor.is_modified)
    self._update_title()

  @Gtk.Template.Callback('note-content-changed')
  def _on_note_content_change(self, _: NoteEditor) -> None:
    """Template handler."""
    self.log.low()
    self._update_rendered()

  @Gtk.Template.Callback('selected-node-changed')
  def _on_nav_selection_change(self, _: NavPane) -> None:
    """Template handler."""
    self.log.low()
    node = self.nav_pane.get_selected_node()
    if isinstance(node, Note):
      self.note_editor.note = node
    else:
      self.note_editor.note = None
    action_service.set_enabled('remove-object', node is not None)
    self.note_viewer.reset_scroll_state()
    self._update_title()

  def new_folder(self, _: Gio.Action, __: None) -> None:
    """Action handler"""
    self.log.msg()
    self.nav_pane.add_new_node(Folder())
    Database.commit_changes()

  def new_note(self, _: Gio.Action, __: None) -> None:
    """Action handler"""
    self.log.msg()
    self.nav_pane.add_new_node(Note())
    Database.commit_changes()

  def remove_object(self, _: Gio.Action, __: None) -> None:
    """Action handler"""
    node = self.nav_pane.get_selected_node()
    if not node:
      raise Exception('Cannot remove: No node selected')
    else:
      dialog = Gtk.Dialog(title='Confirm delete')
      dialog.set_transient_for(self)
      dialog.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
      dialog.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
      box = dialog.get_content_area()
      box.set_property('spacing', 20)
      box.add(Gtk.Label('Are you sure you want to delete %s "%s"' % (node.__class__.__name__, node.title)))
      box.show_all()
      response = dialog.run()
      dialog.destroy()
      if response == Gtk.ResponseType.OK:
        self.nav_pane.remove_node(node)
        node.remove()
        # must do last: this removes node's id but nav_pane needs id to do removal
        Database.commit_changes()

  def save_note(self, _: Gio.Action, __: None) -> None:
    """Action handler"""
    self.note_editor.save_note()

  def hide_nav_pane(self, _: Gio.Action, __: None) -> None:
    self.root_paned.set_property('position', 0)
