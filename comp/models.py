from __future__ import annotations  # allow forward references in annotations. remove in python4.0
import copy
import itertools
import os
import time
import traceback
import warnings
from abc import abstractmethod
from typing import Set, List, Type, TypeVar, Tuple, Dict, Any, Optional, Union, Generic, ClassVar, Callable, Iterator, cast, FrozenSet

import comp.file_op
from comp.config import Config
from comp.base_model import BaseModel, ValidationError, LoadingStrategy, Field, Database


class MultipleOwnershipError(ValidationError):
  pass


class SelfOwnershipError(ValidationError):
  pass


class _RelationshipCache(object):
  """
  This object tracks all parent-child relationships between Folder/Note and Folder/Folder.

  This is required for two features:
    1. Ensuring no invalid relationships are added like circular ownership, or multiple ownership
    2. Ensuring that removed objects are removed from their parent's children
  """
  note_id_to_parent_id: Dict[int, int] = {}
  folder_id_to_parent_id: Dict[int, int] = {}
  indent = ''

  @staticmethod
  def reset() -> None:
    _RelationshipCache.note_id_to_parent_id.clear()
    _RelationshipCache.folder_id_to_parent_id.clear()

  @staticmethod
  def cache_folder(folder: Folder) -> None:
    _RelationshipCache._add_folder_child_notes(folder.id, folder.child_note_ids)
    _RelationshipCache._add_folder_child_folders(folder.id, folder.child_folder_ids)

  @staticmethod
  def notify_note_removed(note: Note) -> None:
    pid = _RelationshipCache.note_id_to_parent_id.get(note.id, None)
    if pid is not None:
      parent = Folder.get(pid)
      # below change triggers hook which updates cache so we dont need to explicitly do it here.
      parent.child_note_ids = tuple(id for id in parent.child_note_ids if id != note.id)

  @staticmethod
  def notify_folder_removed(folder: Folder) -> None:
    # if removed folder has a parent, update it's parent's children
    pid = _RelationshipCache.folder_id_to_parent_id.get(folder.id, None)
    if pid is not None:
      parent = Folder.get(pid)
      # below change triggers hook which updates cache so we dont need to explicitly do it here.
      parent.child_folder_ids = tuple(id for id in parent.child_folder_ids if id != folder.id)

    # also remove mapping from removed folder's children to folder
    _RelationshipCache._remove_folder_child_notes(folder.id, folder.child_note_ids)
    _RelationshipCache._remove_folder_child_folders(folder.id, folder.child_folder_ids)

  @staticmethod
  def _remove_folder_child_notes(parent_id: int, child_note_ids: Tuple[int,...]) -> None:
    for id in child_note_ids:
      pid = _RelationshipCache.note_id_to_parent_id.pop(id)
      assert pid == parent_id  # this should never be false unless API has major bug

  @staticmethod
  def _add_folder_child_notes(parent_id: int, child_note_ids: Tuple[int,...]) -> None:
    for id in child_note_ids:
      if (old_parent_id := _RelationshipCache.note_id_to_parent_id.get(id, None)) is not None:
        raise MultipleOwnershipError('Expected note %d to be unowned but has parent %s' % (id, str(old_parent_id)))
      _RelationshipCache.note_id_to_parent_id[id] = parent_id

  @staticmethod
  def _remove_folder_child_folders(parent_id: int, child_folder_ids: Tuple[int,...]) -> None:
    for id in child_folder_ids:
      pid = _RelationshipCache.folder_id_to_parent_id.pop(id)
      assert pid == parent_id  # this should never be false unless API has major bug

  @staticmethod
  def _add_folder_child_folders(parent_id: int, child_folder_ids: Tuple[int,...]) -> None:
    _RelationshipCache._circular_ownership_check(parent_id, child_folder_ids)
    for id in child_folder_ids:
      if (old_parent_id := _RelationshipCache.folder_id_to_parent_id.get(id, None)) is not None:
        raise MultipleOwnershipError('Expected folder %d to be unowned but has parent %s' % (id, str(old_parent_id)))
      _RelationshipCache.folder_id_to_parent_id[id] = parent_id

  @staticmethod
  def _circular_ownership_check(pid: int, child_ids: Tuple[int,...]) -> None:
    if pid in child_ids:
      raise SelfOwnershipError()
    elif (next_pid := _RelationshipCache.folder_id_to_parent_id.get(pid, None)) is not None:
      _RelationshipCache._circular_ownership_check(cast(int, next_pid), child_ids)

  @staticmethod
  def update_folder_child_folders(parent: BaseModel, old_child_folder_ids: Tuple, new_child_folder_ids: Tuple) -> None:
    _RelationshipCache._remove_folder_child_folders(parent.id, old_child_folder_ids)
    _RelationshipCache._add_folder_child_folders(parent.id, new_child_folder_ids)

  @staticmethod
  def update_folder_child_notes(parent: BaseModel, old_child_note_ids: Tuple, new_child_note_ids: Tuple) -> None:
    # TODO: can do this more efficiently (maybe) by checking removed/added'
    _RelationshipCache._remove_folder_child_notes(parent.id, old_child_note_ids)
    _RelationshipCache._add_folder_child_notes(parent.id, new_child_note_ids)

  @staticmethod
  def change_folder_parent(folder_id: int, new_parent_id: Optional[int]) -> None:
    if (old_parent_id := _RelationshipCache.folder_id_to_parent_id.get(folder_id, None)) is not None:
      old_parent = Folder.get(cast(int, old_parent_id))
      # below triggers cache update so we don't have to do that manually
      old_parent.child_folder_ids = tuple(id for id in old_parent.child_folder_ids if id != folder_id)
    if new_parent_id:
      new_parent = Folder.get(new_parent_id)
      new_parent.child_folder_ids = tuple(itertools.chain(new_parent.child_folder_ids, (folder_id,)))

  @staticmethod
  def change_note_parent(note_id: int, new_parent_id: Optional[int]) -> None:
    if (old_parent_id := _RelationshipCache.note_id_to_parent_id.get(note_id, None)) is not None:
      old_parent = Folder.get(cast(int, old_parent_id))
      # below triggers cache update so we don't have to do that manually
      old_parent.child_note_ids = tuple(id for id in old_parent.child_note_ids if id != note_id)
    if new_parent_id:
      new_parent = Folder.get(new_parent_id)
      new_parent.child_note_ids = tuple(itertools.chain(new_parent.child_note_ids, (note_id,)))


class Node(BaseModel):
  __columns__: ClassVar[Dict[str, Field]] = {
    'title': Field(str, required=True),
  }
  title: str

  @abstractmethod
  def change_parent(self, new_parent_id: Optional[int]) -> None:
    pass

  @abstractmethod
  def get_parent_id(self) -> Optional[int]: pass

@Database.register_table_class
class Note(Node, table_name='note', loading_strategy=LoadingStrategy.IMMEDIATE):
  __columns__: ClassVar[Dict[str, Field]] = {
    'created_date': Field(float, read_only=True, required=True),
    'modified_date': Field(float, read_only=True, required=True)
  }
  created_date: float
  modified_date: float

  def __init__(self, _existing_object_id: int=None, **column_values: Any) -> None:
    if not _existing_object_id:
      column_values['title'] = column_values.get('title', 'Untitled note')
      column_values['created_date'] = time.time()
      column_values['modified_date'] = time.time()

    super().__init__(_existing_object_id=_existing_object_id, **column_values)
    if not _existing_object_id:
      NoteConnector.write_markdown_file(self, '# %s\n' % self.title)

  def remove(self) -> None:
    NoteConnector.remove_markdown_file(self)
    _RelationshipCache.notify_note_removed(self)
    super().remove()

  def _set_modified(self) -> None:
    super()._set_modified()
    self.__dict__['modified_date'] = time.time()

  def change_parent(self, new_parent_id: Optional[int]) -> None:
    super().change_parent(new_parent_id)
    _RelationshipCache.change_note_parent(self.id, new_parent_id)

  def get_parent_id(self) -> Optional[int]:
    return _RelationshipCache.note_id_to_parent_id.get(self.id, None)


@Database.register_table_class
class Folder(Node, table_name='folder', loading_strategy=LoadingStrategy.IMMEDIATE):
  __columns__: ClassVar[Dict[str, Field]] = {
    'child_folder_ids': Field(tuple, default=tuple(), required=True,
        setattr_hook=_RelationshipCache.update_folder_child_folders),
    'child_note_ids': Field(tuple, default=tuple(), required=True,
        setattr_hook=_RelationshipCache.update_folder_child_notes)
  }
  child_folder_ids: Tuple[int,...]
  child_note_ids: Tuple[int,...]

  def __init__(self, _existing_object_id: int=None, **column_values: Any) -> None:
    column_values['title'] = column_values.get('title', 'Untitled folder')
    super().__init__(_existing_object_id=_existing_object_id, **column_values)
    _RelationshipCache.cache_folder(self)

  @classmethod
  def _reset_class_state(cls) -> None:
    super(Node, cls)._reset_class_state()
    _RelationshipCache.reset()

  def _validate(self) -> None:
    # TODO: make a class validation method that avoids having to run this O(n) algorithm n times. (Just globally check for uniqueness of parents instead of recursively doing this for each element)
    super()._validate()
    for id in self.child_note_ids:
      if not Note.has_record_in_session(id):
        raise ValidationError()
    for id in self.child_folder_ids:
      if not Folder.has_record_in_session(id):
        raise ValidationError()

    # Important: decided that we should not check for multiple-ownership or circular references here
    #   1. would be untestable w/o breaking encapsulation since this check is done by __setattr__
    #   2. despite this, cannot be complete. For example if folder structure is:
    #        Folder(1) -> child_folders( 3 )
    #        Folder(2) -> child_folders( 3 )
    #      but only Folder(2) is modified, this will not be caught since only Folder(2) is validated
    #      during commit.

  def remove(self) -> None:
    _RelationshipCache.notify_folder_removed(self)
    super().remove()

  def change_parent(self, new_parent_id: Optional[int]) -> None:
    super().change_parent(new_parent_id)
    _RelationshipCache.change_folder_parent(self.id, new_parent_id)

  def get_parent_id(self) -> Optional[int]:
    return _RelationshipCache.folder_id_to_parent_id.get(self.id, None)

class NoteConnector(object):
  """
  Thin wrapper around reading/writing methods for markdown files.

  This class primarily exists to make tests much easier to write.
  """
  @classmethod
  def write_markdown_file(cls, note: Note, contents: str) -> None:
    filename = Config.get().get_note_markdown_file(note.id)
    comp.file_op.write_file(filename, contents)

  @classmethod
  def read_markdown_file(cls, note: Note) -> str:
    filename = Config.get().get_note_markdown_file(note.id)
    return comp.file_op.read_file(filename)

  @classmethod
  def remove_markdown_file(cls, note: Note) -> None:
    filename = Config.get().get_note_markdown_file(note.id)
    comp.file_op.rename_to_temp(filename, ignore_missing=True)

def register_all() -> None:
  Database.register_table_class(Note)
  Database.register_table_class(Folder)

def unregister_all() -> None:
  Database.unregister_table_class(Note)
  Database.unregister_table_class(Folder)
