import os


def exists(filename: str, include_broken_link: bool=True) -> bool:
  """Checks if filename exists or is broken symbolic link."""
  return os.path.exists(filename) or (include_broken_link and os.path.islink(filename))


def ensure_parent_exists(filename: str) -> None:
  """Creates the parent directory if it doesn't exist."""
  if not os.path.exists(os.path.dirname(filename)):
    os.makedirs(os.path.dirname(filename))


def get_temp_file_name(filename: str) -> str:
  return filename + '.tmp'


def rename_to_temp(filename: str, ignore_missing: bool=False) -> None:
  src = filename
  dst = get_temp_file_name(src)
  if exists(filename):
    ensure_parent_exists(dst)
    os.rename(src, dst)
  elif not ignore_missing:
    raise FileNotFoundError()


def read_file(filename: str) -> str:
  with open(filename, 'r') as f:
    return f.read()


def write_file(filename: str, contents: str) -> None:
  ensure_parent_exists(filename)
  rename_to_temp(filename, ignore_missing=True)
  with open(filename, 'w') as f:
    f.write(contents)
