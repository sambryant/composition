APPLICATION_TITLE = 'Composition'
APPNAME = 'comp'
APPID = 'sjb.comp'
__version__ = '0.1.3'


class NotInitializedError(Exception):
  pass

class AlreadyInitializedError(Exception):
  pass
