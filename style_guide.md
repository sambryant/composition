# Composition Style Guide

## Error handling

There are a few rules regarding error handling:

1. Never return a value on an error, always raise an `Exception`. If a method returns a value, assume it worked correctly
2. In general prefer using custom `Exception` subclasses so calling method can obtain specific information about what went wrong.
3. When running a condition check, if that condition should NEVER occur, use an assert statement rather than testing condition and raising an exception. See 'Assertion/Exception' section

### Assertion/Exception

When writing an API that needs to check that the state of something has the correct value, on failure, we should raise an `Exception` if the state mismatch was the result of the misuse of the API. When the state mismatch indicates that the API itself is programmed incorrectly, prefer `assert`.

Example:

Exception on API misuse:

    def remove_object(self):
      if self.state == removed:
        # if caller misuses API by calling obj.remove() twice, we raise an Exception
        raise Exception() twice

Assert on API programming mistake:

    def remove_object(self):
      ... # do remove routine
      # if remove_object is written correctly, the following should always be true, so use assert
      assert self.state == removed

When in doubt, use the behavior of `base_model.py` as a model.

**Why this convention?**

1. Testing: statements that can never occur (checked by `assert`) by definition cannot be unit tested without breaking encapsulation. Thus if you use `if (...): raise Exception`, it will show incomplete unit test coverage because the `Exception` statement is never reached.
2. Fail-Fast: We still want the assert statement because if there is a bug in the API, a timely `assert` statement will reveal this quickly, allowing us to fix the bug before it leads to mysterious side effects
3. Readability: raising an `Exception` indicates that something leading to this method was done incorrectly (or there is an external system error). This help signposts how the API should and should not be used. On the other hand, `assert` tells reader that something must necessarily be true regardless.
