import warnings
from typing import Any, Type, Dict, Union
from pytest import raises, fixture
from unittest.mock import patch

from tests.mock_base_model_writer import mock_base_model_writer
from comp.config import Config
from comp.base_model import Field, BaseModel, BaseModelWriter, ReadOnlyError, NotLoadedError, InvalidIDError, StateError, Database, NotInitializedError, CorruptError, ValidationError, NotRegisteredError, LoadingStrategy


@Database.register_table_class
class ModelA(BaseModel, table_name='model_a'):
  __columns__ = {
    'normal_field': Field(str, default='default')
  }


@Database.register_table_class
class ModelB(BaseModel, table_name='model_b'):
  __columns__ = {
    'required_field': Field(str, required=True),
    'readonly_field':  Field(str, read_only=True),
    'default_field':  Field(str, default='default value'),
    'normal_field': Field(str)
  }
  _non_field_attr: str


@Database.register_table_class
class ModelAChild(ModelA, table_name='model_a_child'):
  __columns__ = {
    'child_field': Field(str, default='child value')
  }


class NonTableClass(BaseModel):
  __columns__ = {
    'column1': Field(int, default=1),
    'column2': Field(str, default='default value')
  }


@fixture(autouse=True)
def global_setup_teardown():
  Config.initialize()

  yield

  # ignore 'test only method' warning from `deinitialize`
  with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    Config.deinitialize()

  Database.reset()


class TestBaseModel(object):

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  def test_repr(self, mock_writer):
    mock_base_model_writer(mock_writer, {ModelA: {1: {}}})
    Database.initialize()

    assert str(ModelA()) == 'ModelA(2)'

  def test_column_inheritance(self):
    child_cols = ModelAChild.get_columns()
    print(child_cols)
    assert 'normal_field' in child_cols
    assert 'child_field' in child_cols
    assert 'child_field' not in ModelA.get_columns()

  class TestNonTableClass(object):

    def test_has_correct_columns(self):
      cols = NonTableClass.get_columns()
      assert len(cols) == 2
      assert cols['column1'] == Field(int, default=1)
      assert cols['column2'] == Field(str, default='default value')

    def test_has_loading_strategy(self):
      assert NonTableClass.get_loading_strategy() == LoadingStrategy.EXPLICIT

    def test_raises_not_registered_errors(self):
      with raises(NotRegisteredError):
        NonTableClass.get_table_name()
      with raises(NotRegisteredError):
        NonTableClass.get(1)
      with raises(NotRegisteredError):
        NonTableClass.get_instance_map()
      with raises(NotRegisteredError):
        NonTableClass.get_all()
      with raises(NotRegisteredError):
        NonTableClass.get_loaded_ids()
      with raises(NotRegisteredError):
        NonTableClass.get_session_ids()
      with raises(NotRegisteredError):
        NonTableClass.get_added()
      with raises(NotRegisteredError):
        NonTableClass.get_modified()
      with raises(NotRegisteredError):
        NonTableClass.get_removed()
      with raises(NotRegisteredError):
        NonTableClass.has_record_in_database(1)
      with raises(NotRegisteredError):
        NonTableClass.has_record_in_session(1)
      with raises(NotRegisteredError):
        NonTableClass.has_model_instance(1)


  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestSetAttr(object):

    def test_can_change_private_attribute(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1, 2}
      Database.initialize()
      obj = ModelB(required_field='value 1')
      obj._random_attribute = 'value'
      with raises(AttributeError):
        obj.random_attribute = 'value'

    def test_cannot_change_id(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1, 2}
      Database.initialize()
      obj = ModelB(required_field='value 1')
      with raises(AttributeError):
        obj.id = 8

    def test_normal_field(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1, 2}
      Database.initialize()
      obj = ModelB(required_field='value 1')
      obj.normal_field = 'new value'
      assert obj.normal_field == 'new value'

    def test_unknown_attr(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1, 2}
      Database.initialize()
      obj = ModelB(required_field='value 1')
      with raises(AttributeError):
        obj.bad_field = 'thing'

    def test_parent_attr(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1, 2}
      Database.initialize()
      obj = ModelAChild()
      assert obj.normal_field == 'default'
      obj.normal_field = 'new value'
      assert obj.normal_field == 'new value'

    def test_id(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1, 2}
      Database.initialize()

      obj = ModelB(required_field='value 1')
      assert obj.id == 3
      with raises(AttributeError):
        obj.id = 4

    def test_readonly(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1, 2}
      Database.initialize()
      obj = ModelB(required_field='value 1', readonly_field='some value')
      assert obj.readonly_field == 'some value'
      with raises(ReadOnlyError):
        obj.readonly_field = 'new value'

    def test_after_remove(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}, 2: {}}})
      Database.initialize()

      obj = ModelA.get(2, allow_load=True)

      obj.normal_field = 'new value'
      obj.remove()
      with raises(StateError):
        obj.normal_field = 'newer value'

    def test_can_set_private_attributes(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}, 2: {}}})
      Database.initialize()

      obj = ModelA()

      with raises(AttributeError): obj.readonly_field = 'new value'

      obj._non_field_attr = 'new value' # okay

      with raises(AttributeError): obj.non_private_attr = 'new value'

      assert not obj.is_modified()

    def test_can_set_private_attributes_after_remove(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}, 2: {}}})
      Database.initialize()

      obj = ModelA.get(2, allow_load=True)
      obj.normal_field = 'new value'
      obj.remove()

      with raises(StateError):
        obj.normal_field = 'new value'
      obj._non_field_attr = 'new value'


  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestInit(object):
    """
    Tests creating new records via BaseModel() (automatically added to session as new record.)
    """

    def test_created_in_session(self, mock_writer):
      mock_base_model_writer(mock_writer, {})
      Database.initialize()

      assert not ModelA.get_session_ids()
      assert not ModelA.has_record_in_session(1)
      assert ModelA().id == 1
      assert ModelA.get_session_ids() == {1}
      assert ModelA.has_record_in_session(1)

    def test_missing_required_field(self, mock_writer):
      mock_base_model_writer(mock_writer, {})
      Database.initialize()

      with raises(AttributeError):
        obj = ModelB()

      obj = ModelB(required_field='value')

    def test_default_attribute_value(self, mock_writer):
      mock_base_model_writer(mock_writer, {})
      Database.initialize()

      obj = ModelB(required_field='value')
      assert obj.default_field == 'default value'

    def test_missing_attribute_defaults_to_none(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelB: {1: {}, 2: {}}})
      Database.initialize()

      obj = ModelB(required_field='value')
      assert obj.normal_field is None

    def test_attributes_set_as_expected(self, mock_writer):
      mock_base_model_writer(mock_writer, {})
      Database.initialize()

      obj1 = ModelB(required_field='value 1')
      assert obj1.required_field == 'value 1'
      assert obj1.readonly_field is None
      assert obj1.default_field == 'default value'
      assert obj1.normal_field is None

    def test_attributes_set_explicit(self, mock_writer):
      mock_base_model_writer(mock_writer, {})
      Database.initialize()

      obj1 = ModelB(
        required_field='value 1',
        readonly_field='value 2',
        default_field='value 3',
        normal_field='value 4')

      assert obj1.required_field == 'value 1'
      assert obj1.readonly_field == 'value 2'
      assert obj1.default_field == 'value 3'
      assert obj1.normal_field == 'value 4'

    def test_readonly_field(self, mock_writer):
      mock_base_model_writer(mock_writer, {})
      Database.initialize()

      obj1 = ModelB(required_field='value 1', readonly_field='some value')
      assert obj1.required_field == 'value 1'
      assert obj1.readonly_field == 'some value'

    def test_bad_attributes(self, mock_writer):
      mock_base_model_writer(mock_writer, {})
      Database.initialize()

      with raises(AttributeError):
        ModelB(required_field='value', bad_field='value 1')

    def test_use_next_id(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {}, 19: {}}
      })
      Database.initialize()

      assert not ModelA.has_record_in_session(20)
      obj = ModelA()
      assert ModelA.has_record_in_session(20)
      assert obj.id == 20

    def test_classes_use_separate_ids(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {}, 19: {}},
        ModelB: {1: {}, 5: {}}
      })
      Database.initialize()

      assert ModelA().id == 20
      assert ModelB(required_field='value').id == 6

    def test_increment_id(self, mock_writer):
      mock_base_model_writer(mock_writer, {})
      Database.initialize()

      obj1 = ModelA()
      obj2 = ModelA()
      assert obj1.id == 1
      assert obj2.id == 2

    def test_id_uses_largest_database_id(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {}, 9: {}}
      })
      Database.initialize()

      assert ModelA().id == 10

    def test_id_reuse_on_creation_failure(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {}}
      })
      Database.initialize()

      assert ModelA().id == 3 # next after 2
      with raises(AttributeError): ModelA(invalid_field='hi')
      assert ModelA().id == 4 # next after 3, despite failure
      assert ModelA.get_session_ids() == {1, 2, 3, 4}

    def test_id_skip_on_load_failure(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {'invalid_field': 'causes error'}}
      })
      Database.initialize()

      with raises(AttributeError): ModelA.get(2, allow_load=True)
      assert ModelA().id == 3 # does not reuse 2, despite failure

    def test_id_skip_on_removal_of_existing(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {}}
      })
      Database.initialize()

      ModelA.get(2, allow_load=True).remove()
      assert not ModelA.has_record_in_session(2)
      assert ModelA().id == 3  # new object skips id=2 even though removed

    def test_id_skip_on_removal_of_created(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}}
      })
      Database.initialize()

      assert ModelA().id == 2
      ModelA.get(2).remove()
      assert ModelA().id == 3  # new object skips id=2 even though removed

    def test_id_reuse_removal_after_commit(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {}}
      })
      Database.initialize()

      ModelA.get(2, allow_load=True).remove()
      assert ModelA().id == 3  # new object skips id=2 even though removed
      ModelA.get(3).remove()
      Database.commit_changes()
      assert ModelA().id == 2  # new object now uses id=2

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestGet(object):

    def test_get_basic(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {'normal_field': 'loaded value'}, 2: {}}
      })
      Database.initialize()

      assert not ModelA.has_model_instance(1)
      obj = ModelA.get(1, allow_load=True)  # loads from filesystem
      obj = ModelA.get(1, allow_load=True)  # does not load from filesystem since already loaded
      mock_writer.read_record_file.assert_called_once_with(ModelA, 1)
      assert obj.normal_field == 'loaded value'
      assert ModelA.has_model_instance(1)

    def test_get_missing_record(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {
          1: {'normal_field': 'loaded value'},
          2: FileNotFoundError(),
          3: CorruptError()
        }
      })
      Database.initialize()
      assert ModelA.get(1, allow_load=True).normal_field == 'loaded value'

      with raises(FileNotFoundError):
        ModelA.get(2, allow_load=True)

      with raises(CorruptError):
        ModelA.get(3, allow_load=True)

      assert ModelA().id == 4 # doesn't reuse, even though invalid

    def test_created_object_automatically_loaded(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelB: {1: {'required_field': 'value 1'}}
      })
      Database.initialize()

      # create two objects, then load all three objects, check that record load only called once.
      obj2 = ModelB(required_field='value 2')
      obj3 = ModelB(required_field='value 3')
      assert ModelB.get(1, allow_load=True).id == 1
      assert ModelB.get(obj2.id, allow_load=False) == obj2
      assert ModelB.get(obj3.id, allow_load=True) == obj3
      mock_writer.read_record_file.assert_called_once_with(ModelB, 1)

    def test_get_not_loaded_error(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}}})
      Database.initialize()

      # without 'allow_load' argument, trying to get an unloaded object raises error
      with raises(NotLoadedError):
        ModelA.get(1)
      ModelA.get(1, allow_load=True)
      ModelA.get(1)
      mock_writer.read_record_file.assert_called_once()

    def test_get_invalid_record(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {}}
      })
      Database.initialize()

      with raises(InvalidIDError):
        ModelA.get(9, allow_load=True)
      mock_writer.read_record_file.assert_not_called()

    def test_get_loads_bad_record(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelB: {
          1: {'required_field': 'some value', 'invalid_field': 'value'}, # invalid field
          2: {}, # missing required
          3: {'required_field': 'some value'}, # goldilocks object
        }})
      Database.initialize()
      assert ModelB.get_session_ids() == {1, 2, 3}

      with raises(AttributeError):
        ModelB.get(1, allow_load=True)
      with raises(AttributeError):
        ModelB.get(2, allow_load=True)
      ModelB.get(3, allow_load=True)

      assert set(ModelB.get_instance_map().keys()) == {3} # only valid object put in map


  class TestGetInstanceMap(object):

    def test(self):
      warnings.warn('todo')


  class TestGetLoadedIds(object):

    def test(self):
      warnings.warn('todo')

  class TestGetSessionIds(object):

    def test(self):
      warnings.warn('todo')

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestHasRecordInDatabase(object):

    def test_basic(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2,9}
      Database.initialize()
      assert ModelA.has_record_in_database(1)
      assert ModelA.has_record_in_database(2)
      assert ModelA.has_record_in_database(9)

    def test_exclude_added(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      Database.initialize()
      obj3 = ModelA()
      assert not ModelA.has_record_in_database(obj3.id)

    def test_include_removed(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      mock_writer.read_record_file.return_value = {}
      Database.initialize()
      o = ModelA.get(2, allow_load=True)

      assert ModelA.has_record_in_database(2)
      o.remove()
      assert ModelA.has_record_in_database(2)

    def test_include_added_after_commit(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      Database.initialize()
      obj3 = ModelA()

      assert not ModelA.has_record_in_database(obj3.id)
      Database.commit_changes()
      assert ModelA.has_record_in_database(obj3.id)

    def test_exclude_removed_after_commit(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      mock_writer.read_record_file.return_value = {}
      Database.initialize()
      o = ModelA.get(2, allow_load=True)

      assert ModelA.has_record_in_database(2)
      o.remove()
      assert ModelA.has_record_in_database(2)
      Database.commit_changes()
      assert not ModelA.has_record_in_database(2)

    def test_invalid_id_okay(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      Database.initialize()

      assert not ModelA.has_record_in_database(3)

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestHasRecordInSession(object):

    def test_basic(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2,9}
      Database.initialize()
      assert ModelA.has_record_in_session(1)
      assert ModelA.has_record_in_session(2)
      assert ModelA.has_record_in_session(9)

    def test_include_added(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      Database.initialize()
      obj3 = ModelA()
      assert ModelA.has_record_in_session(obj3.id)

    def test_exclude_removed(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      mock_writer.read_record_file.return_value = {}
      Database.initialize()
      o = ModelA.get(2, allow_load=True)

      assert ModelA.has_record_in_database(2)
      o.remove()
      assert not ModelA.has_record_in_session(2)

    def test_invalid_id_okay(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      Database.initialize()

      assert not ModelA.has_record_in_session(3)

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestRecordIsLoaded(object):

    def test_not_loaded(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      Database.initialize()
      assert not ModelA.has_model_instance(2)

    def test_explicit_load(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      Database.initialize()

      assert not ModelA.has_model_instance(2)
      ModelA.get(2, allow_load=True)
      assert ModelA.has_model_instance(2)

    def test_added_are_loaded(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      Database.initialize()

      ModelA()
      assert ModelA.has_model_instance(3)

    def test_invalid_id_okay(self, mock_writer):
      mock_writer.read_record_list_file.return_value = {1,2}
      Database.initialize()

      assert not ModelA.has_model_instance(3) # doesnt raise error even though not a valid record


  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestRemove(object):

    def test_removed_from_session(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      assert ModelA.has_record_in_session(3)
      ModelA.get(3, allow_load=True).remove()
      assert not ModelA.has_record_in_session(3)

    def test_not_removed_from_database(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      assert ModelA.has_record_in_database(3)
      ModelA.get(3, allow_load=True).remove()
      assert ModelA.has_record_in_database(3)

    def test_id_not_reused(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      ModelA.get(3, allow_load=True).remove()
      assert ModelA().id == 4

    def test_remove_twice_error(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      obj = ModelA.get(3, allow_load=True)
      obj.remove()
      with raises(StateError):
        obj.remove()

    def test_cant_set_attr(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      obj = ModelA.get(3, allow_load=True)
      obj.normal_field = 'new value'
      obj.remove()
      with raises(StateError):
        obj.normal_field = 'newer value'

    def test_overrides_modification_state(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      obj = ModelA.get(3, allow_load=True)
      obj.normal_field = 'new value'
      assert ModelA.get_modified() == {obj}
      obj.remove()
      assert ModelA.get_modified() == set()

    def test_overrides_added(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      obj = ModelA()
      assert ModelA.get_added() == {obj}
      obj.remove()
      assert ModelA.get_added() == set()

    def test_removes_file_on_commit(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}, 3: {}}})
      Database.initialize()
      ModelA.get_all()

      ModelA.get(3).remove()
      mock_writer.remove_record_file.assert_not_called()
      Database.commit_changes()
      mock_writer.remove_record_file.assert_called_once_with(ModelA, 3)

    def test_no_validation_before_commit(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelB: {
          1: {'required_field': 'value 1'},
          2: {'required_field': 'value 2'}
        }})
      Database.initialize()

      # remove an object with invalid column type and it will not cause error
      obj = ModelB.get(1, allow_load=True)
      obj.required_field = 1234 # incorrect type
      obj.remove()
      Database.commit_changes()
      assert not ModelB.has_record_in_database(1)

      # try to save an object with modified column with bad type and it will cause error
      obj = ModelB.get(2, allow_load=True)
      obj.required_field = 1234 # incorrect type
      with raises(ValidationError):
        Database.commit_changes()

    def test_state_errors_after_commiting_remove(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}, 3: {}}})
      Database.initialize()
      ModelA.get_all()

      obj = ModelA.get(3)
      obj.remove()
      Database.commit_changes()
      with raises(StateError):
        obj.remove()
      with raises(StateError):
        obj.normal_field = 'new value'


  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestGetRemoved(object):

    def test_basic(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}, 2: {}}})
      Database.initialize()

      obj = ModelA.get(2, allow_load=True)
      obj.remove()
      ModelA.get_removed() == {obj}


  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestGetAdded(object):

    def test_get_added(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      obj1 = ModelA()
      obj2 = ModelA()
      obj3 = ModelA()
      assert ModelA.get_added() == {obj1, obj2, obj3}

    def test_ignore_modified(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {}}
      })
      Database.initialize()

      o1 = ModelA.get(1, allow_load=True)
      o3 = ModelA()
      o4 = ModelA()
      o1.normal_field = 'new value'
      assert ModelA.get_added() == {o3, o4}

    def test_ignore_removed(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      o3 = ModelA()
      o4 = ModelA()
      o5 = ModelA()
      o4.remove()
      assert ModelA.get_added() == {o3, o5}


  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestGetAll(object):

    def test_basic(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      assert not ModelA.has_model_instance(1)
      assert not ModelA.has_model_instance(3)
      all_objs = ModelA.get_all()
      assert ModelA.has_model_instance(1)
      assert ModelA.has_model_instance(3)
      assert len(all_objs) == 2

    def test_exclude_removed(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      assert len(ModelA.get_all()) == 2
      ModelA.get(3, allow_load=True).remove()
      assert len(ModelA.get_all()) == 1

    def test_include_added(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 3: {}}
      })
      Database.initialize()

      assert len(ModelA.get_all()) == 2
      ModelA()
      assert len(ModelA.get_all()) == 3


  class TestGetModified(object):

    def test(self):
      warnings.warn('not implemented yet')


  class TestGetSessionIds(object):
    """
    Important b/c used to determine what to save to record list.
    """
    def test(self):
      warnings.warn('not implemented yet')


  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestCommit(object):

    def test_basic_features(self, mock_writer):
      """
      Kind of a catchall test for basic functionality. Tests a few things:
        1. Modified and added objects are all written to filesystem DB
        2. Removed objects are removed from the filesystem DB
        3. Modified/Removed/Added objects are reset after commit.
      """
      mock_base_model_writer(mock_writer, {
        ModelA: {
          1: {},
          2: {'normal_field': 'original value'},
          3: {'normal_field': 'to delete'}
        }})
      Database.initialize()
      ModelA.get_all()

      assert not ModelA.get_modified()
      assert not ModelA.get_added()
      assert not ModelA.get_removed()

      obj2, obj3 = ModelA.get(2), ModelA.get(3)
      obj4 = ModelA()          # added
      obj2.normal_field = 'new value' # modified
      obj3.remove()             # deleted

      assert ModelA.get_modified() == {obj2}
      assert ModelA.get_removed() == {obj3}
      assert ModelA.get_added() == {obj4}

      Database.commit_changes()

      # check that record list itself was saved
      mock_writer.write_record_list_file.assert_called_once_with(ModelA, {1,2,4})
      assert ModelA.get_session_ids() == {1, 2, 4}

      # check that added/modified objects were written, check that removed object was removed
      assert mock_writer.write_record_file.call_count == 2
      mock_writer.write_record_file.assert_any_call(ModelA, 2, {'normal_field': 'new value'})
      mock_writer.remove_record_file.assert_called_once_with(ModelA, 3)
      mock_writer.write_record_file.assert_any_call(ModelA, 4, {'normal_field': 'default'})

      # check that all object states were reset
      assert not ModelA.get_modified()
      assert not ModelA.get_added()
      assert not ModelA.get_removed()

      # check that objects are all in session+database
      for id in [1, 2, 4]:
        assert ModelA.has_record_in_session(id)
        assert ModelA.has_record_in_database(id)
        assert ModelA.has_model_instance(id)
      for id in [3]:
        assert not ModelA.has_record_in_session(id)
        assert not ModelA.has_record_in_database(id)
        assert not ModelA.has_model_instance(id)

      mock_writer.cleanup.assert_called_once_with()

    def test_clears_removed(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {}, 3: {}}
      })
      Database.initialize()
      ModelA.get_all()

      obj1, obj3 = ModelA.get(1), ModelA.get(3)
      obj1.remove()
      obj3.remove()

      assert ModelA.get_removed() == {obj1, obj3}
      Database.commit_changes()
      assert ModelA.get_removed() == set()

    def test_removed_objects_remove_record(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {
        1: {},
        2: {'normal_field': 'original value'},
      }})
      Database.initialize()
      ModelA.get_all()

      ModelA.get(2).remove()

      assert ModelA.get_session_ids() == {1}
      Database.commit_changes()
      mock_writer.write_record_list_file.assert_called_once_with(ModelA, {1})
      mock_writer.remove_record_file.assert_called_once_with(ModelA, 2)

    def test_removing_created_doesnt_remove_record(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}}})
      Database.initialize()
      ModelA.get_all()

      ModelA().remove()

      Database.commit_changes()
      mock_writer.write_record_list_file.assert_not_called()
      mock_writer.remove_record_file.assert_not_called()

      ModelA.get(1).remove()

      Database.commit_changes()
      mock_writer.write_record_list_file.assert_called_once()
      mock_writer.remove_record_file.assert_called_once()

    def test_removed_objects_lose_id(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {
        1: {},
        2: {'normal_field': 'original value'},
      }})
      Database.initialize()
      ModelA.get_all()

      obj2 = ModelA.get(2)
      obj2.remove()

      assert obj2.id == 2
      Database.commit_changes()
      with raises(AttributeError):
        obj2.id

    def test_removed_reuse_id_after_commit(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {
        1: {},
        2: {'normal_field': 'original value'},
      }})
      Database.initialize()
      ModelA.get_all()

      ModelA.get(2).remove()
      assert ModelA().id == 3 # increments id even though id=2 is removed
      ModelA.get(3).remove()

      Database.commit_changes()
      assert ModelA().id == 2 # same id as deleted object

    def test_removed_all_next_id_is_one(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {}}
      })
      Database.initialize()
      ModelA.get_all()

      ModelA.get(1).remove()
      ModelA.get(2).remove()

      Database.commit_changes()
      assert ModelA().id == 1

    def test_removed_objects_not_in_db(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {1: {}, 2: {}}
      })
      Database.initialize()
      ModelA.get_all()

      ModelA.get(2).remove()
      assert ModelA.has_record_in_database(2)
      Database.commit_changes()
      assert not ModelA.has_record_in_database(2)

    def test_clears_modified(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {
        1: {},
        2: {'normal_field': 'original value'},
      }})
      Database.initialize()
      ModelA.get_all()
      ModelA.get(1).normal_field = 'new value 1'
      ModelA.get(2).normal_field = 'new value 2'

      assert ModelA.get_modified() == {ModelA.get(1), ModelA.get(2)}
      assert ModelA.get(1).is_modified()
      assert ModelA.get(2).is_modified()
      Database.commit_changes()
      assert not ModelA.get_modified()
      assert not ModelA.get(1).is_modified()
      assert not ModelA.get(2).is_modified()

    def test_modified_writes_record(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {
        1: {'normal_field': 'original value 1'},
        2: {'normal_field': 'original value 2'},
      }})
      Database.initialize()
      ModelA.get_all()
      ModelA.get(2).normal_field = 'new value 2'

      Database.commit_changes()
      mock_writer.write_record_file.assert_called_once_with(ModelA, 2, {'normal_field': 'new value 2'})

    def test_modified_no_list_write(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {
        1: {},
        2: {'normal_field': 'original value'},
      }})
      Database.initialize()
      ModelA.get_all()

      ModelA.get(1).normal_field = 'new value 1'
      ModelA.get(2).normal_field = 'new value 2'

      Database.commit_changes()
      mock_writer.write_record_list_file.assert_not_called()
      mock_writer.write_record_file.assert_any_call(ModelA, 1, {'normal_field': 'new value 1'})
      mock_writer.write_record_file.assert_any_call(ModelA, 2, {'normal_field': 'new value 2'})
      assert mock_writer.write_record_file.call_count == 2

    def test_clears_added(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {
        1: {},
        2: {'normal_field': 'original value'},
      }})
      Database.initialize()
      ModelA.get_all()

      obj3, obj4 = ModelA(), ModelA()

      assert ModelA.get_added() == {obj3, obj4}
      Database.commit_changes()
      assert not ModelA.get_added()

    def test_added_writes_record(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {
        1: {'normal_field': 'original value 1'},
        2: {'normal_field': 'original value 2'},
      }})
      Database.initialize()
      ModelA.get_all()

      ModelA(normal_field='value 3')

      Database.commit_changes()
      mock_writer.write_record_file.assert_called_once_with(ModelA, 3, {'normal_field': 'value 3'})

    def test_validate_missing_column_in_added(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}, 2: {}}})
      Database.initialize()
      ModelA.get_all()

      obj3 = ModelA()
      del obj3.__dict__['normal_field']
      with raises(ValidationError):
        Database.commit_changes()
      mock_writer.write_record_list_file.assert_not_called()
      mock_writer.write_record_file.assert_not_called()
      assert ModelA.get_added() == {obj3}

    def test_no_changes_on_validation_error(self, mock_writer):
      mock_base_model_writer(mock_writer, {
        ModelA: {
          1: {'normal_field': 'untouched'},
          2: {'normal_field': 'to change'},
          3: {'normal_field': 'to remove'}
        }})
      Database.initialize()
      ModelA.get_all()

      obj1, obj2, obj3 = ModelA.get(1), ModelA.get(2), ModelA.get(3)
      obj2.normal_field = 1234.1234 # triggers error cause not a str
      obj3.remove()
      obj4 = ModelA()
      assert ModelA.get_modified() == {obj2}
      assert ModelA.get_removed() == {obj3}
      assert ModelA.get_added() == {obj4}

      with raises(ValidationError):
        Database.commit_changes()
      assert ModelA.get_modified() == {obj2}
      assert ModelA.get_removed() == {obj3}
      assert ModelA.get_added() == {obj4}

    def test_validate_missing_column_in_modified(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}, 2: {}}})
      Database.initialize()
      ModelA.get_all()

      obj2 = ModelA.get(2)
      obj2.normal_field = 'newer value'
      del obj2.__dict__['normal_field']
      assert ModelA.get_modified() == {obj2}
      with raises(ValidationError):
        Database.commit_changes()
      mock_writer.write_record_list_file.assert_not_called()
      mock_writer.write_record_file.assert_not_called()
      assert ModelA.get_modified() == {obj2}

    def test_validate_nonrequired_field_can_be_none(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}, 2: {}}})
      Database.initialize()
      ModelA.get_all()

      obj2 = ModelA.get(2)
      obj2.normal_field = None
      Database.commit_changes()
      mock_writer.write_record_file.assert_called_once_with(ModelA, 2, {'normal_field': None})

    def test_validate_incorrect_value_type(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {}, 2: {}}})
      Database.initialize()
      ModelA.get_all()

      obj2 = ModelA.get(2)
      obj2.normal_field = 15 # should be string
      with raises(ValidationError):
        Database.commit_changes()

    def test_validate_missing_required_column_in_added(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelB: {1: {'required_field': 'required 1'}, 2: {'required_field': 'required 2'}}})
      Database.initialize()
      ModelB.get_all()

      obj3 = ModelB(required_field='required 3')
      obj3.required_field = None

      with raises(ValidationError):
        Database.commit_changes()
      mock_writer.write_record_list_file.assert_not_called()
      mock_writer.write_record_file.assert_not_called()
      assert ModelB.get_added() == {obj3}

    def test_validate_missing_required_column_in_modified(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelB: {1: {'required_field': 'required 1'}, 2: {'required_field': 'required 2'}}})
      Database.initialize()
      ModelB.get_all()

      ModelB.get(2).required_field = None

      with raises(ValidationError):
        Database.commit_changes()
      mock_writer.write_record_list_file.assert_not_called()
      mock_writer.write_record_file.assert_not_called()
      assert ModelB.get_modified() == {ModelB.get(2)}


  @patch('comp.base_model.BaseModelWriter', autospec=True)
  class TestIsModified():

    def test_basic(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {'normal_field': 'original value'}}})
      Database.initialize()
      ModelA.get_all()

      obj = ModelA.get(1)
      assert not obj.is_modified()
      assert obj not in ModelA.get_modified()
      obj.normal_field = 'new value'
      assert obj.is_modified()
      assert obj in ModelA.get_modified()

    def test_created_never_modified(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {'normal_field': 'original value'}}})
      Database.initialize()
      ModelA.get_all()

      obj = ModelA()
      assert not obj.is_modified()
      assert obj not in ModelA.get_modified()
      obj.normal_field = 'new value'
      assert not obj.is_modified()
      assert obj not in ModelA.get_modified()

    def test_removed_never_modified(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {'normal_field': 'original value'}}})
      Database.initialize()
      ModelA.get_all()

      obj = ModelA.get(1)
      obj.normal_field = 'new value'
      assert obj.is_modified()
      assert obj in ModelA.get_modified()
      obj.remove()
      assert not obj.is_modified()
      assert obj not in ModelA.get_modified()

    def test_false_after_commit(self, mock_writer):
      mock_base_model_writer(mock_writer, {ModelA: {1: {'normal_field': 'original value'}}})
      Database.initialize()
      ModelA.get_all()

      obj = ModelA.get(1)
      obj.normal_field = 'new value'

      assert obj.is_modified()
      assert obj in ModelA.get_modified()
      Database.commit_changes()
      assert not obj.is_modified()
      assert obj not in ModelA.get_modified()


class TestBaseModelWriter(object):

  @patch('comp.file_op.read_file', autospec=True)
  @patch('comp.config.Config.get_record_list_filename', autospec=True)
  class TestReadRecordListFile(object):

    def test_correct_file_patterns(self, mock_get_record_list_filename, mock_read_file):
      mock_get_record_list_filename.return_value = 'record_file.record'
      mock_read_file.return_value = '1,2,3,4,5'
      ids = BaseModelWriter.read_record_list_file(ModelA)
      mock_get_record_list_filename.assert_called_once_with(Config.get(), 'model_a')
      mock_read_file.assert_called_once_with('record_file.record')
      assert ids == {1,2,3,4,5}

    def test_ignores_duplicates(self, mock_get_record_list_filename, mock_read_file):
      mock_get_record_list_filename.return_value = 'record_file.record'
      mock_read_file.return_value = '1,1,2,2,3'
      assert BaseModelWriter.read_record_list_file(ModelA) == {1,2,3}

    def test_raises_corrupt_error(self, mock_get_record_list_filename, mock_read_file):
      mock_get_record_list_filename.return_value = 'record_file.record'
      mock_read_file.return_value = '1.0,cat'
      with raises(CorruptError):
        BaseModelWriter.read_record_list_file(ModelA) == {1,2,3}

  @patch('comp.file_op.read_file', autospec=True)
  @patch('comp.config.Config.get_record_filename', autospec=True)
  class TestReadRecordFile(object):

    def test_correct_file_patterns(self, mock_get_filename, mock_read_file):
      mock_get_filename.return_value = 'table/1.info'
      mock_read_file.return_value = '{"normal_field": "some value"}'
      assert BaseModelWriter.read_record_file(ModelA, 1) == {'normal_field': 'some value'}
      mock_get_filename.assert_called_once_with(Config.get(), 'model_a', 1)
      mock_read_file.assert_called_once_with('table/1.info')

    def test_raises_corrupt_error(self, mock_get_filename, mock_read_file):
      mock_get_filename.return_value = 'table/1.info'
      mock_read_file.return_value = '{"bad_json: "some value"}'
      with raises(CorruptError):
        BaseModelWriter.read_record_file(ModelA, 1)

  @patch('comp.file_op.read_file')
  @patch('comp.file_op.write_file', autospec=True)
  @patch('comp.config.Config.get_record_list_filename', autospec=True)
  class TestWriteRecordListFile(object):

    def test_correct_file_patterns(self, mock_get_record_list_filename, mock_write_file, mock_read_file):
      mock_get_record_list_filename.return_value = 'record_file.record'

      # check that roundtrip returns same id set.
      BaseModelWriter.write_record_list_file(ModelA, {1,2,3,4,7})
      mock_get_record_list_filename.assert_called_once_with(Config.get(), 'model_a')
      mock_write_file.assert_called_once()

      # set read to return file contents of file written by above call
      mock_read_file.return_value = mock_write_file.mock_calls[0].args[1]
      assert BaseModelWriter.read_record_list_file(ModelA) == {1,2,3,4,7}

  class TestWriteRecordFile(object):

    @patch('comp.file_op.read_file')
    @patch('comp.file_op.write_file', autospec=True)
    @patch('comp.config.Config.get_record_filename', autospec=True)
    def test_roundtrip(self, mock_get_filename, mock_write_file, mock_read_file):
      mock_get_filename.return_value = 'table/1.info'
      obj_dict = {
        'str_field': 'normal value',
        'bool_field': True,
        'float_field': 1234.5678,
        'int_field': 1
      }
      # save object dict, load object dict and check that we get same dict back
      BaseModelWriter.write_record_file(ModelA, 1, obj_dict)

      mock_get_filename.assert_called_once_with(Config.get(), 'model_a', 1)
      mock_write_file.assert_called_once()
      output_contents = mock_write_file.mock_calls[0].args[1]

      mock_read_file.return_value = output_contents
      actual_dict = BaseModelWriter.read_record_file(ModelA, 1)

      assert actual_dict == obj_dict

  @patch('comp.file_op.rename_to_temp', autospec=True)
  @patch('comp.config.Config.get_record_filename', autospec=True)
  def test_remove_record_file(self, mock_get_record_filename, mock_rename_to_temp):
    mock_get_record_filename.return_value = 'record_file.record'
    BaseModelWriter.remove_record_file(ModelA, 1)
    mock_get_record_filename.assert_called_once_with(Config.get(), 'model_a', 1)
    mock_rename_to_temp.assert_called_once_with('record_file.record')

  def test_errors(self):
    warnings.warn('Need to write tests dealing with read/write warnings')


class TestDatabase(object):

  def test_cannot_be_instantiated(self):
    with raises(Exception):
      Database()

  def test_has_registered_class(self):
    assert Database.has_registered_class(ModelA)
    assert not Database.has_registered_class(NonTableClass)

  def test_registered_class_missing_table_name(self):
    class TestModel(BaseModel):
      pass
    with raises(Exception):
      Database.register_table_class(TestModel)

  def test_reregister_class(self):
    class TestModel(BaseModel, table_name='test_model'):
      pass
    Database.register_table_class(TestModel)
    with raises(Exception):
      Database.register_table_class(TestModel)
    Database.unregister_table_class(TestModel)

  def test_duplicate_table_name(self):
    class TestModel1(BaseModel, table_name='test_model'):
      pass
    class TestModel2(BaseModel, table_name='test_model'):
      pass
    Database.register_table_class(TestModel1)
    with raises(Exception):
      Database.register_table_class(TestModel2)
    Database.unregister_table_class(TestModel1)

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  def test_initialize(self, mock_writer):
    mock_writer.read_record_list_file.return_value = set()
    assert not ModelA.is_initialized()
    Database.initialize()
    assert ModelA.is_initialized()

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  def test_initialize_twice(self, mock_writer):
    mock_writer.read_record_list_file.return_value = set()
    Database.initialize()
    with raises(Exception):
      Database.initialize()

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  def test_loads_database_ids(self, mock_writer):
    mock_writer.read_record_list_file.return_value = {1,2}

    Database.initialize()

    mock_writer.read_record_list_file.assert_any_call(ModelA)
    assert ModelA.has_record_in_database(1)
    assert ModelA.has_record_in_database(2)
    assert set(ModelA.get_session_ids()) == {1, 2}

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  def test_uses_empty_list_when_missing_file(self, mock_writer):
    mock_writer.read_record_list_file.side_effect = FileNotFoundError

    assert not ModelA.is_initialized()
    Database.initialize()
    assert ModelA.is_initialized()
    assert set(ModelA.get_session_ids()) == set()

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  def test_corrupt_record_list(self, mock_writer):
    mock_writer.read_record_list_file.side_effect = CorruptError

    with raises(CorruptError):
      Database.initialize()
      assert not ModelA.is_initialized()

  def test_not_init_errors(self):
    with raises(NotInitializedError):
      ModelA()
    with raises(NotInitializedError):
      ModelA.get(1)
    with raises(NotInitializedError):
      ModelA()
    with raises(NotInitializedError):
      ModelA.get_added()
    with raises(NotInitializedError):
      ModelA.get_modified()
    with raises(NotInitializedError):
      ModelA.get_removed()
    with raises(NotInitializedError):
      ModelA.has_record_in_database(1)
    with raises(NotInitializedError):
      ModelA.has_record_in_session(1)
    with raises(NotInitializedError):
      ModelA.has_model_instance(1)
    with raises(NotInitializedError):
      ModelA.get_instance_map()
    with raises(NotInitializedError):
      ModelA.get_instance_map()
    with raises(NotInitializedError):
      ModelA.get_session_ids()


class TestLoadingStrategy(object):

  class ExplicitLoad(BaseModel, loading_strategy=LoadingStrategy.EXPLICIT, table_name='explicit_load'):
    __columns__ = {
      'normal_field': Field(str, default='explicit')
    }


  class ImmediateLoad(BaseModel, loading_strategy=LoadingStrategy.IMMEDIATE, table_name='immediate_load'):
    __columns__ = {
      'normal_field': Field(str, default='immediate')
    }

  def setup(self):
    Database.register_table_class(TestLoadingStrategy.ExplicitLoad)
    Database.register_table_class(TestLoadingStrategy.ImmediateLoad)

  def teardown(self):
    Database.unregister_table_class(TestLoadingStrategy.ImmediateLoad)
    Database.unregister_table_class(TestLoadingStrategy.ExplicitLoad)

  def test_get_loading_strategy(self):
    assert TestLoadingStrategy.ExplicitLoad.get_loading_strategy() == LoadingStrategy.EXPLICIT
    assert TestLoadingStrategy.ImmediateLoad.get_loading_strategy() == LoadingStrategy.IMMEDIATE

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  def test_immediate_load(self, mock_writer):
    mock_base_model_writer(mock_writer, {
      TestLoadingStrategy.ExplicitLoad: {1: {}, 2: {}},
      TestLoadingStrategy.ImmediateLoad: {1: {}, 2: {}}
    })
    Database.initialize()

    assert TestLoadingStrategy.ImmediateLoad.get_loaded_ids() == {1, 2}
    assert TestLoadingStrategy.ExplicitLoad.get_loaded_ids() == set()
    mock_writer.read_record_file.assert_any_call(TestLoadingStrategy.ImmediateLoad, 1)
    mock_writer.read_record_file.assert_any_call(TestLoadingStrategy.ImmediateLoad, 2)
    assert mock_writer.read_record_file.call_count == 2


class TestField(object):

  @Database.register_table_class
  class FieldModel(BaseModel, table_name='field_model'):
    __columns__ = {
      'int_field': Field(int),
      'str_field': Field(str),
      'float_field': Field(float),
      'bool_field': Field(bool),
      'tuple_field': Field(tuple)
    }

  @patch('comp.config.Config.get_record_filename', return_value='field_model/1.info')
  @patch('comp.config.Config.get_record_list_filename', return_value='field_model/master.records')
  @patch('comp.file_op.write_file', autospec=True)
  @patch('comp.file_op.read_file', autospec=True)
  def test_load_save_round_trip(self, mock_read_file, mock_write_file, mock_get_record_list_filename, mock_get_record_filename):
    # setup empty database
    mock_read_file.return_value = ''
    Database.initialize()

    # create a new object, write its record to file and commit
    obj = TestField.FieldModel()
    Database.commit_changes()
    for mock in [mock_read_file, mock_write_file, mock_get_record_list_filename, mock_get_record_filename]:
      mock.reset_mock()

    # fill object fields and write it to database, record the output file contents
    obj.int_field = 1
    obj.str_field = 'hi'
    obj.float_field = 1.2
    obj.bool_field = True
    obj.tuple_field = (1,2,3,4)
    Database.commit_changes()
    mock_write_file.assert_called_once()
    record_file_contents = mock_write_file.mock_calls[0].args[1]

    # reset database letting it think there is a single record
    Database.reset()
    mock_read_file.return_value = '1'
    Database.initialize()

    # set the record file contents of single record to be what was output earlier, check fields unchanged.
    mock_read_file.return_value = record_file_contents
    new_obj = TestField.FieldModel.get(1, allow_load=True)

    assert new_obj.int_field == 1
    assert new_obj.str_field == 'hi'
    assert new_obj.float_field == 1.2
    assert new_obj.bool_field == True
    assert new_obj.tuple_field == (1,2,3,4)

  def test_default_has_bad_type(self):
    with raises(Exception):
      f = Field(str, default=1)

  def test_eq_comparison(self):
    assert Field(int) == Field(int)
    assert Field(int) != Field(str)
    assert Field(int, required=True) != Field(int, required=False)
    assert Field(int, default=1) != Field(int, default=2)
    assert Field(int, default=None) != Field(int, default=0)
    assert Field(int, read_only=True) != Field(int)
    # different setattr hooks
    def setattr_hook(obj, old, new):
      pass
    assert Field(int, setattr_hook=setattr_hook) != Field(int)
    # different object types
    class SubField(Field):
      pass
    assert Field(int) != SubField(int)
